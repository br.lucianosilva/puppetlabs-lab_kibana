define lab_kibana::files(
    $serverport = "5601",
    $serverhost = 'localhost',
    $elasticsearchurl = 'localhost:9200',
    $loggingdest = 'stdout',
){
	File {
		ensure => file,
		owner => "root",
		group => "root",
		mode => "0644",
	}
	file {"/etc/kibana/${title}":
		content => template("lab_kibana/${title}.erb"),
		notify => Service['kibana'],
	}
}
