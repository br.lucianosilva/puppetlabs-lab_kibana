class lab_kibana::config {
    
### Resources ###

    if $::os['family'] == 'Redhat' {
   	notify {'SucessNotify':
	    message => "Operation System OK...",
	} 
        
	package {'kibana':
            ensure => "present",
            require => File['/etc/yum.repos.d/kibana.repo'],
        }
        file {'/etc/yum.repos.d/kibana.repo':
            ensure => file,
            source => "puppet:///modules/lab_kibana/kibana.repo",
	    mode => "0644",
        }
    
    } else {
	notify {'FailedNotify':
	   message => "Operation System Not Fould...",
	}
    } 
   
   lab_kibana::files {'kibana.yml':
	 elasticsearchurl => "localhost:9200",
	 loggingdest => "stdout",
	 serverhost => "0.0.0.0",
         serverport => "5601",

   }
   
   service {'kibana':
   	ensure => "running",
   }
}

